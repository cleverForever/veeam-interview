## The second task was chosen as a test task

###Command line options

  `-h, --help`           show this help message and exit

  `--source SOURCE`      This is path to source

  `--target TARGET`      This is path to replica

  `--interval INTERVAL`  Synchronization interval (sec)

  `--log LOG`            Path to log file
  