import argparse
import logging

from dirsync import sync

from Repeater import Repeat

parser = argparse.ArgumentParser(description='Command line options')

parser.add_argument('--source', required=True, type=str, help='This is path to source')
parser.add_argument('--target', required=True, type=str, help='This is path to replica')
parser.add_argument('--interval', type=float, default=10, help='Synchronization interval (sec)')
parser.add_argument('--log', type=str, help='Path to log file')
args = parser.parse_args()

file_log = ''
console_out = logging.StreamHandler()
handlers = [console_out]
if args.log:
    file_log = logging.FileHandler(args.log)
    handlers.append(file_log)

logging.basicConfig(
    handlers=handlers,
    format='[%(asctime)s | %(levelname)s]: %(message)s',
    datefmt='%m.%d.%Y %H:%M:%S',
    level=logging.INFO,
)


def periodical():
    sync(args.source, args.target, 'sync', vebrose=True, purge=True, create=True, logger=logging)


if __name__ == '__main__':
    timer = Repeat(args.interval, periodical)
    timer.start()
